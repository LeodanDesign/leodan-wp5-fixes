<?php

/*
Plugin Name: Leodan WP beta gutenberg init mimicry
Description: Replaces the functions that are present in the Gutenberg plugin but not in the WP Beta 5.0
Version: 0.1
Author: leodandesign
License: Help Yourself Mateys!!!
*/

    if (!function_exists('gutenberg_can_edit_post_type')) {

        function gutenberg_can_edit_post_type($post_type)
        {
            return true;
        }
    }

    if (!function_exists('gutenberg_init')) {
        function gutenberg_init()
        {
            return true;
        }
    }
    if (!function_exists( 'the_gutenberg_project' )){
        function the_gutenberg_project(){
            return true;
        }
    }